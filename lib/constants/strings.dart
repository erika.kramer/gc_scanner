class Strings {
  static String appbar_headline_Events = 'Events';
  static String appbar_headline_Event = 'Event';
  static String button_ticket_scannen = 'Ticket Scannen';
  static String button_sign_in = 'Sign In';
  static String graphics_golden_logo = 'assets/images/graphics_golden_logo.webp';
  static String allert_dialog_no_success = 'ACCESS NOT GRANTED';
  static String allert_dialog_success = 'ACCESS GRANTED';
  static String events_url = 'http://192.168.178.29:3000/events';
  static String event_url = 'http://192.168.178.29:3000/event';
  static String ticket_url = 'http://192.168.178.29:3000/ticket';
  static String tickets_url = 'http://192.168.178.29:3000/tickets';
  static String login_url = 'http://192.168.178.29:3000/login';
  static String event_url_neusta = 'http://192.168.0.246:3000/event';
  static String tickets_url_neusta = 'http://192.168.0.246:3000/tickets';
  static String ticket_url_neusta = 'http://192.168.0.246:3000/ticket';
}

