import 'package:intl/intl.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gc_scanner/screens/event_screen.dart';
import 'package:gc_scanner/theme/zeplin_colors.dart';
import '../models/events.dart';
import '../theme/zeplin_text_style.dart';

class EventListCard extends StatelessWidget {
  @override
  Events events;

  EventListCard(this.events);

  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 15, right: 15),
      child: Container(
        height: 110,
        width: 328,
        child: InkWell(onTap: ()=>  Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => EventScreen())),
        child: Card(
          color: ZeplinColors.white,
          child: Row(
            children: [
              Container(
                  height: 100,
                  width: 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      image: DecorationImage(
                        image: NetworkImage(events.imageUrl),
                        fit: BoxFit.cover,
                      ))),
              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 11, left: 11),
                    child: Row(
                      children: [
                        const Padding(
                          padding: EdgeInsets.only(right: 8),
                          child: SizedBox(
                              height: 22,
                              width: 20,
                              child: Icon(
                                Icons.calendar_today,
                                color: Colors.black,
                              )),
                        ),
                        Text(
                          DateFormat('dd-MM-yyyy').format(events.terminVon),
                          style: ZeplinTextStyles.CalendarDayBlack,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 11, left: 11),
                    child: Row(
                      children: [
                        const Padding(
                          padding: EdgeInsets.only(right: 8),
                          child: SizedBox(
                            height: 22,
                            width: 20,
                            child: Icon(
                              Icons.access_time,
                              color: Colors.black,
                            ),
                          ),
                        ),
                        Text(
                          DateFormat('hh:mm').format(events.terminVon),
                          style: ZeplinTextStyles.CalendarDayBlack,
                        ),
                        const Text(
                          ' - ',
                          style: ZeplinTextStyles.CalendarDayBlack,
                        ),
                        Text(
                          DateFormat('hh:mm').format(events.terminBis),
                          style: ZeplinTextStyles.CalendarDayBlack,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 11, left: 11),
                    child: Text(events.titel),
                  )
                ],
              )
            ],
          ),
        ),
      ),
      ),);
  }
}
