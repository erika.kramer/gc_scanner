import 'package:flutter/material.dart';
import 'package:gc_scanner/models/event_model.dart';
import 'package:gc_scanner/theme/zeplin_colors.dart';
import 'package:gc_scanner/theme/zeplin_text_style.dart';
import 'package:intl/intl.dart';

import 'diagramm_alert.dart';

class EventCard extends StatelessWidget {
  Event event;

  EventCard(this.event);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ZeplinColors.scaffold_background,
      child: Column(
        children: [
          Stack(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 84, left: 30, right: 30),
      child: InkWell(onTap: ()=> _popupDialog(context),

                child: Container(
                  height: 332,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    image: DecorationImage(
                      image: NetworkImage(event.imageUrl),
                      fit: BoxFit.cover,
                      colorFilter: ColorFilter.mode(
                          Colors.black.withOpacity(0.8), BlendMode.dstATop),
                    ),
                  ),
                  child: Padding(
                    padding:
                        const EdgeInsets.only(top: 255, left: 16, right: 16),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            const Icon(
                              Icons.calendar_today,
                              color: Colors.white,
                              size: 24,
                            ),
                            Text(
                              DateFormat('yyyy-MM-dd').format(event.terminVon),
                              style: ZeplinTextStyles.CalendarDayWhite,
                            ),
                            Expanded(child: Container()),
                            const Icon(
                              Icons.access_time,
                              color: Colors.white,
                              size: 24,
                            ),
                            Text(
                              DateFormat('hh:mm').format(event.terminVon),
                              style: ZeplinTextStyles.CalendarDayWhite,
                            ),
                            Text(
                              ' - ',
                              style: ZeplinTextStyles.CalendarDayWhite,
                            ),
                            Text(
                              DateFormat('hh:mm').format(event.terminBis),
                              style: ZeplinTextStyles.CalendarDayWhite,
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 16),
                          child: Row(
                            children: [
                              Text(
                                event.titel.toUpperCase(),
                                style:
                                    ZeplinTextStyles.HeadlineSecondaryWhite20SP,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
              ),],
          ),
        ],
      ),
    );
  }

  void _popupDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (context){
          return DiagramAlert();
        }
    );
  }
}
