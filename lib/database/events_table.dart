import 'package:drift/drift.dart';


@DataClassName('Event')
class EventsTable extends Table{
  IntColumn get eventID => integer().autoIncrement()();
  TextColumn get titel => text()();
  DateTimeColumn get terminVon => dateTime()();
  DateTimeColumn get terminBis => dateTime()();
  TextColumn get imageUrl => text()();
  IntColumn get anzahlGesamtTickets => integer()();
  IntColumn get anzahlTicketkaufOnline => integer().withDefault(const Constant(0))();
  IntColumn get anzahlTicketkaufKasse => integer().withDefault(const Constant(0))();
  IntColumn get gaesteAktuell => integer().withDefault(const Constant(0))();


  @override
  Set<Column> get primaryKey => {eventID};
}