import 'package:drift/drift.dart';
//import '../models/login.dart';
import 'app_database.dart';
import 'base_repo.dart';
import 'login_table.dart';

class LoginRepo extends BaseRepo<LoginTable, Login> {
  LoginRepo(AppDatabase appDb) : super(appDb);

  @override
  TableInfo<LoginTable, Login> get table => appDb.loginTable;

  Future<Login> getLogin() {
    return appDb.select(table).getSingle();
  }
}
