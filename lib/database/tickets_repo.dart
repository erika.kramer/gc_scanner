import 'package:drift/drift.dart';
import 'package:gc_scanner/database/tickets_table.dart';
//import '../models/ticket.dart';
import 'app_database.dart';
import 'base_repo.dart';

class TicketsRepo extends BaseRepo<TicketsTable, Ticket> {
  TicketsRepo(AppDatabase appDb) : super(appDb);

  TableInfo<TicketsTable, Ticket> get table => appDb.ticketsTable;

  Future<Ticket> getTicketById(int id) {
    return (appDb.select(table)..where((ticket) => ticket.ticketID.equals(id)))
        .getSingle();
  }

  Future<Ticket> getTicketQRCode(String qr) {
    return (appDb.select(table)..where((ticket) => ticket.qrCode.equals(qr)))
        .getSingle();
  }

  Future<List<Ticket>> getTicketsByEvent(int id) {
    return (appDb.select(table)..where((ticket) => ticket.eventID.equals(id)))
        .get();
  }

  Stream<List<Ticket>> getTicketsByEventasStream(int id) {
    return (appDb.select(table)..where((ticket) => ticket.eventID.equals(id)))
        .watch();
  }
}
