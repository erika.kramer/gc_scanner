import 'package:drift/drift.dart';
import 'app_database.dart';

abstract class BaseRepo<T extends Table, D extends DataClass> {
  AppDatabase appDb;

  TableInfo<T, D> get table;

  BaseRepo(this.appDb);

  Future<List<D>> getAll() {
    return appDb.select(table).get();
  }

  Stream<List<D>> getAllAsStream() {
    return appDb.select(table).watch();
  }

  Future<int> insert(Insertable<D> item) {
    return appDb.into(table).insert(item);
  }

  Future<void> insertAll(List<Insertable<D>> items) {
    return appDb.batch((batch) {
      batch.insertAll(table, items);
    });
  }

  Future<void> insertOrUpdate(List<Insertable<D>> items) {
    return appDb.batch((batch) {
      batch.insertAll(table, items, mode: InsertMode.replace);
    });
  }

  Future<int> update(Insertable<D> item) {
    return appDb.into(table).insert(item, mode: InsertMode.replace);
  }

  Future<int> delete(Insertable<D> item) {
    return appDb.delete(table).delete(item);
  }

  Future<int> deleteAllData() {
    return appDb.delete(table).go();
  }
}
