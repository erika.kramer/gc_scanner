import 'package:drift/drift.dart';

@DataClassName('Login')
class LoginTable extends Table{
  IntColumn get loginID => integer()();
  TextColumn get username => text()();
  TextColumn get password => text()();

  @override
  Set<Column> get primaryKey => {loginID};
}