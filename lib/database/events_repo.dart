import 'package:drift/drift.dart';
//import 'package:gc_scanner/models/event_model.dart';
import 'app_database.dart';
import 'base_repo.dart';
import 'events_table.dart';

class EventsRepo extends BaseRepo<EventsTable, Event> {
  EventsRepo(AppDatabase appDb) : super(appDb);

  @override
  TableInfo<EventsTable, Event> get table => appDb.eventsTable;

  Future<Event> getEventById(int id) {
    return (appDb.select(table)..where((event) => event.eventID.equals(id)))
        .getSingle();
  }

  Future<List<Event>> getCurrentEvents(DateTime currentTime) {
    return (appDb.select(table)
          // bigger than value because we want to get the events in the future
          ..where((e) => e.terminVon.isBiggerThanValue(currentTime)))
        .get();
  }

  Stream<List<Event>> getCurrentEventsAsStream(DateTime currentTime) {
    return (appDb.select(table)
          // bigger than value because we want to get the events in the future
          ..where((e) => e.terminVon.isBiggerThanValue(currentTime)))
        .watch();
  }
}
