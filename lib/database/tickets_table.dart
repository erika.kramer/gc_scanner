import 'package:drift/drift.dart';

@DataClassName('Ticket')
class TicketsTable extends Table {
  IntColumn get ticketID => integer().autoIncrement()();
  TextColumn get qrCode => text()();
  BoolColumn get ticketScanned =>
      // changed to false => tickets are not scanned in the beginning
      boolean().withDefault(const Constant(false))();
  IntColumn get eventID => integer()();

  @override
  Set<Column> get primaryKey => {ticketID};
}
