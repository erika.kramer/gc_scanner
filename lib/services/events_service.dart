import 'package:gc_scanner/constants/strings.dart';
import 'package:gc_scanner/models/events.dart';
import 'base_service.dart';

class EventsService extends ApiBaseService {
  Future<List<Events>> getEvents() async {
    dynamic eventsListJson = await getData(Strings.events_url);
    final List<Events> events = eventsFromJson(eventsListJson);
    print(events);
    return events;
  }
}
