import 'package:gc_scanner/constants/strings.dart';
import 'package:gc_scanner/models/event_model.dart';
import 'base_service.dart';

class EventService extends ApiBaseService {
  Future<Event> getEvent() async {
    dynamic eventJson = await getData(Strings.event_url);
    final Event event = eventFromJson(eventJson);
    print(event);
    return event;
  }
}
