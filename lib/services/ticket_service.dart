import 'package:gc_scanner/constants/strings.dart';
import 'package:gc_scanner/models/ticket.dart';
import 'base_service.dart';

class TicketService extends ApiBaseService {
  Future<Ticket> getTicket() async {
    dynamic ticketJson = await getData(Strings.ticket_url);
    final Ticket ticket = ticketFromJson(ticketJson);
    print(ticket);
    return ticket;
  }
}
