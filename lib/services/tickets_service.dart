import 'package:gc_scanner/constants/strings.dart';
import 'package:gc_scanner/models/tickets.dart';
import 'base_service.dart';

class TicketsService extends ApiBaseService {
  Future<List<Tickets>> getTickets() async {
    dynamic ticketsListJson = await getData(Strings.tickets_url);
    final List<Tickets> tickets = ticketsFromJson(ticketsListJson);
    print(tickets);
    return tickets;
  }
}