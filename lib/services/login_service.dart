import 'package:gc_scanner/constants/strings.dart';
import 'package:gc_scanner/models/login.dart';
import 'base_service.dart';

class LoginService extends ApiBaseService {
  Future<Login> getLogin() async {
    dynamic loginJson = await getData(Strings.login_url);
    final Login login = loginFromJson(loginJson);
    print(login);
    return login;
  }
}
