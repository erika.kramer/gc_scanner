import 'package:flutter/material.dart';
import 'package:gc_scanner/screens/login_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: (LoginScreen()));
  }
}
