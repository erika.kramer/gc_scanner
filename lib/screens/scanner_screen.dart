import 'package:flutter/material.dart';
import 'package:gc_scanner/constants/strings.dart';
import 'package:gc_scanner/models/ticket.dart';
import 'package:gc_scanner/services/ticket_service.dart';
import 'package:gc_scanner/theme/zeplin_colors.dart';
import 'package:gc_scanner/theme/zeplin_text_style.dart';

class ScannerScreen extends StatelessWidget {
  late String _ticketQRCode;
  String _scanQRcode;

  ScannerScreen(this._scanQRcode);

  Future<Ticket> ticket = TicketService().getTicket();

  String checkQRCode(String qrInput, String qrDatabase) {
    if (qrInput == qrDatabase) {
      return Strings.allert_dialog_success;
    } else {
      return Strings.allert_dialog_no_success;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(65),
          child: AppBar(
            title: Text(
              Strings.appbar_headline_Event.toUpperCase(),
              style: ZeplinTextStyles.HeadlinePrimaryWhite24SP,
            ),
            backgroundColor: ZeplinColors.primary_blue,
          ),),
      body: Container(
        color: Colors.grey,
        child: FutureBuilder(
          future: ticket,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            print(snapshot.hasData);
            if (snapshot.hasData) {
              Ticket ticketData = snapshot.data;
              _ticketQRCode = ticketData.qrCode;
              return Center(
                child: (AlertDialog(
                  title: Text(checkQRCode(
                    _ticketQRCode,
                    _scanQRcode,
                  )),
                )),
              );
            } else {
              return CircularProgressIndicator(
                color: Colors.yellow,
              );
            }
          },
        ),
      ),
    );
  }
}
