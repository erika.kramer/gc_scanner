import 'package:flutter/material.dart';
import 'package:gc_scanner/models/event_model.dart';
import 'package:gc_scanner/services/event_service.dart';
import 'package:gc_scanner/theme/zeplin_colors.dart';
import 'package:gc_scanner/theme/zeplin_text_style.dart';
import 'package:gc_scanner/widgets/event_card.dart';
import 'package:gc_scanner/constants/strings.dart';
import 'package:gc_scanner/widgets/scan_button.dart';

class EventScreen extends StatefulWidget {
  @override
  _EventScreenState createState() => _EventScreenState();
}

class _EventScreenState extends State<EventScreen> {
  late Future<Event> _event;
  String qrCode = 'Unknown';

  @override
  void initState() {
    super.initState();
    _event = EventService().getEvent();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(65),
        child: AppBar(
          title: Text(
            Strings.appbar_headline_Event.toUpperCase(),
            style: ZeplinTextStyles.HeadlinePrimaryWhite24SP,
          ),
          backgroundColor: ZeplinColors.primary_blue,
        ),
      ),
      body: Container(
        child: FutureBuilder(
          future: _event,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            print(snapshot.hasData);
            if (snapshot.hasData) {
              Event event = snapshot.data;
              return Column(
                children: [
                  EventCard(event),
                  ScanButton(),
                ],
              );
            } else {
              return Center(
                child: CircularProgressIndicator(
                  color: Colors.purple,
                ),
              );
            }
          },
        ),
      ),
    );
  }


}
