import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gc_scanner/constants/strings.dart';

import 'package:gc_scanner/services/event_service.dart';
import 'package:gc_scanner/services/events_service.dart';
import 'package:gc_scanner/models/events.dart';
import 'package:gc_scanner/theme/zeplin_colors.dart';
import 'package:gc_scanner/theme/zeplin_text_style.dart';
import '../models/event_model.dart';
import '../widgets/event_list_card.dart';
import 'event_screen.dart';

class EventsScreen extends StatefulWidget {
  @override
  _EventsScreenState createState() => _EventsScreenState();
}

class _EventsScreenState extends State<EventsScreen> {
  late Future<List<Events>> _events;
  String qrCode = 'Unknown';

  @override
  void initState() {
    super.initState();
    _events = EventsService().getEvents();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(65),
        child: AppBar(
          title: Text(
            Strings.appbar_headline_Events.toUpperCase(),
            style: ZeplinTextStyles.HeadlinePrimaryWhite24SP,
          ),
          backgroundColor: ZeplinColors.primary_blue,
          automaticallyImplyLeading: false,
        ),
      ),
      body: Container(padding: const EdgeInsets.only(top: 50), color: ZeplinColors.scaffold_background,
        child: FutureBuilder(
          future: _events,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            print(snapshot.hasData);
            if (snapshot.hasData) {
              List<Events> events = snapshot.data;
              return Container(
                  color: ZeplinColors.scaffold_background,
                  child: ListView.builder(
                    itemCount: events.length,
                    itemBuilder: (context, index) {
                      Events events = snapshot.data[index];
                      return EventListCard(events

                      );
                    },

                  ),);
            } else {
              return Center(
                child: CircularProgressIndicator(
                  color: Colors.purple,
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
